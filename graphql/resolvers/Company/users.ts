import db, {UserRow, CompanyRow} from '../../db'

export default async function users(company: CompanyRow): Promise<UserRow[]> {
  const users: UserRow[] = await db.getAll(
    `
      SELECT u.*
      FROM user_companies up
      JOIN users u ON up.user_id = u.id
      WHERE up.company_id = ?
    `,
    [company.id]
  )
  return users
}
