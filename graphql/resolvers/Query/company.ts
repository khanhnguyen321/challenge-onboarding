import db, {CompanyRow} from '../../db'

type Args = {
  id: number;
}

export default async function company(parent: unknown, {id}: Args): Promise<CompanyRow> {
  const company: CompanyRow | undefined = await db.getOne(
    'SELECT * FROM companies WHERE id = ?',
    [id]
  )
  if (!company) {
    throw new Error(`Company ${id} not found`)
  }
  return company
}
