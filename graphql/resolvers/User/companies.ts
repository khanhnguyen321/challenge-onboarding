import db, {UserRow, CompanyRow} from '../../db'

export default async function companies(user: UserRow): Promise<CompanyRow[]> {
  const companies: CompanyRow[] = await db.getAll(
    `
      SELECT p.*
      FROM user_companies up
      JOIN companies p ON up.company_id = p.id
      WHERE up.user_id = ?
    `,
    [user.id]
  )
  return companies
}
