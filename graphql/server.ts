import {ApolloServer, gql} from 'apollo-server-micro'
import * as resolvers from './resolvers'

const typeDefs = gql`
  type Company {
    id: Int!
    name: String!
    description: String!
    icon_url: String!
    users: [User!]!
  }

  type User {
    id: Int!
    name: String!
    bio: String!
    avatar_url: String!
    group_type: String!
    companies: [Company!]!
  }

  type Query {
    company(id: Int!): Company!
    user(id: Int!): User!
  }
`;

export const server = new ApolloServer({typeDefs, resolvers})
