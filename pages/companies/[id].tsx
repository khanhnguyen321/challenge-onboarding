import {useRouter} from 'next/router'
import {useQuery, gql} from '@apollo/client'
import Layout from 'components/Layout'
import CompanyCard from 'components/CompanyCard'

const COMPANY_QUERY = gql`
  query company($id: Int!) {
    company(id: $id) {
      id
      name
      description
      icon_url
      users {
        id
        name
        avatar_url
      }
    }
  }
`

type QueryData = {
  company: Company;
}

type QueryVars = {
  id: number;
}

type Company = {
  id: number;
  name: string;
  description: string;
  icon_url: string;
  users: User[];
}

type User = {
  id: number;
  name: string;
  avatar_url: string;
}

export default function CompanyPage() {
  const {query} = useRouter()

  const {data, error, loading} = useQuery<QueryData, QueryVars>(
    COMPANY_QUERY,
    {
      skip: !query.id,
      variables: {id: Number(query.id)},
    }
  )
  const company = data?.company;

  if (!company || loading || error) {
    return null
  }

  return (
    <Layout>
      <CompanyCard company={company} />
    </Layout>
  )
}