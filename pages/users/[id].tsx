import {useRouter} from 'next/router'
import {useQuery, gql} from '@apollo/client'
import Layout from 'components/Layout'
import UserCard from 'components/UserCard'

const USER_QUERY = gql`
  query user($id: Int!) {
    user(id: $id) {
      id
      name
      bio
      group_type
      avatar_url
      companies {
        id
        name
        icon_url
      }
    }
  }
`

type QueryData = {
  user: User;
}

type QueryVars = {
  id: number;
}

type User = {
  id: number;
  name: string;
  bio: string;
  group_type: "investors" | "developers" | "gamers" | "testers";
  avatar_url: string;
  companies: Company[];
}

type Company = {
  id: number;
  name: string;
  icon_url: string;
}

export default function UserPage() {
  const {query} = useRouter()

  const {data, error, loading} = useQuery<QueryData, QueryVars>(
    USER_QUERY,
    {
      skip: !query.id,
      variables: {id: Number(query.id)},
    }
  )
  const user = data?.user;

  if (!user || loading || error) {
    return null
  }

  return (
    <Layout>
      <UserCard user={user} />
    </Layout>
  )
}
