# Coding challenge

## Getting started with the repo

1. Clone it: `git clone https://khanhnguyen321@bitbucket.org/khanhnguyen321/challenge-onboarding.git`
2. Open the folder: `cd challenge-onboarding`
3. Install the dependencies: `yarn install`
4. Run the dev server: `yarn dev`
5. Open http://localhost:3000

## Setting: what this toy project is about

This project portrays a made-up Gaming community platform (called OnBoarding). 

We have users participating in four group types:
- Investors
- Developers
- Testers
- Gamers

We want to create a newsfeed for each user type that shows new and relevant events. The goal is to keep users up to date and to facilitate collaboration between them.

In general, there are three types of events:
- new people (DB table `users`)
- new companies (table `companies`)
- team announcements (table `announcements`)

However, each newsfeed should consist of different types of content because people from different types are interested in different events:
- Investors want to connect to developers and other investors.
- Developers want to connect to investors, gamers, testers, and other developers.
- Investors, gamers, and testers are interested in new companies.
- Gamers want to connect only to developers and other gamers.
- Testers like to connect to other testers.

Announcements can be addressed to a specific group type, or to all users (see table `announcements`, column `group_type`). Investors are not interested in announcements addressed to gamers or testers, and so on.

## Coding task

Implement the newsfeed:
- It should include users, companies, and announcements.
- It should display different results, depending on the selected group_type, as described in the "Setting" section above.
- Entries should be sorted by creation date, newer entries go first.
- Implement infinite scrolling, don't download and display all entries at once.

Tips:
- You can change any part of the application — DB connection, GraphQL server/client, styled-components — if you are more comfortable or productive with something else.
- You can change the project structure as you see fit.
- You can add any NPM package you need to implement new features or improve the existing code.
- You can reuse the existing React components, or modify them so they fit better in the newsfeed.
- Don't spend much time creating beautiful UI, just make it look consistent.

## How to submit

1. Include a short write-up walkthrough of the UI and how it works.
2. Zip up the project file (minus unneeded folders: .git, .next, node_modules, etc) and email it back.

## Project structure

Tech stack:
- Next.js
- TypeScript
- Sqlite3
- Apollo server
- Apollo client
- React

Folder structure:
- `components/` — reusable React components
- `pages/` — the usual Next.js [page structure](https://nextjs.org/docs/basic-features/pages)
- `graphql/` — GraphQL server, schema, resolvers, DB connection
- `data/` — Pull in this data instead if you are unfamiliar with GraphQL

The database is already included in the repo (`db.sqlite`). Its basic structure:
- `users` — people participating in group_types;
- `companies` — companies that investors are working on (connected to `users` through `user_companies`)
- `announcements` — announcements by OnBoarding Team targeting specific group_types or global (`group_type = "all"`)
  
