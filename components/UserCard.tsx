import Link from 'next/link'
import styled from 'styled-components'
import Card from './Card'
import Markdown from './Markdown'

type Props = {
  user: User;
}

type User = {
  id: number;
  name: string;
  bio: string;
  group_type: "investors" | "developers" | "gamers" | "testers";
  avatar_url: string;
  companies: Company[];
}

type Company = {
  id: number;
  name: string;
  icon_url: string;
}

export default function UserCard({user}: Props) {
  return (
    <Card>
      <Columns>
        <ColumnLeft>
          <Avatar src={user.avatar_url}/>
        </ColumnLeft>
        <ColumnRight>
          <h2>{user.name}</h2>
          <p>Group Type: {user.group_type}</p>
          <Markdown>{user.bio}</Markdown>
          {!!user.companies.length && (
            <>
              <h3>Companies:</h3>
              {user.companies.map(p => (
                <Company key={p.id} company={p} />
              ))}
            </>
          )}
        </ColumnRight>
      </Columns>
    </Card>
  )
}

const Avatar = styled.img`
  background-color: rgba(0, 0, 0, 0.1);
  border-radius: 10px;
`

const Columns = styled.div`
  display: flex;
  flex-direction: row;
  min-width: 21rem;
`

const ColumnLeft = styled.div`
  display: flex;
  flex-direction: column;
  flex-basis: 7rem;
  flex-grow: 0;
  flex-shrink: 0;
  margin-right: 1.5rem;
`

const ColumnRight = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  flex-shrink: 0;
  flex-basis: 14rem;
`

function Company({company}: {company: Company}) {
  return (
    <CompanyContainer>
      <CompanyColumnLeft>
        <CompanyIcon src={company.icon_url} />
      </CompanyColumnLeft>
      <CompanyColumnRight>
        <Link href={`/companies/${company.id}`}>
          {company.name}
        </Link>
      </CompanyColumnRight>
    </CompanyContainer>
  )
}

const CompanyIcon = styled.img`
  border-radius: 3px;
  background-color: rgba(0, 0, 0, 0.1);
`

const CompanyContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 1rem;
`

const CompanyColumnLeft = styled.div`
  flex-basis: 2rem;
  flex-shrink: 0;
  flex-grow: 0;
  margin-right: 1rem;
`

const CompanyColumnRight = styled.div`
  flex-basis: 3rem;
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
`
